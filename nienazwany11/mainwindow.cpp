#include "mainwindow.h"
// Library needed for processing XML documents
#include <QtXml>
// Library needed for processing files
#include <QFile>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->Opis_chain_kin->setEnabled(false);
    ui->OX->setEnabled(false);
    ui->OY->setEnabled(false);
    ui->OZ->setEnabled(false);
    ui->endChain->setEnabled(false);
 //   druk1();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::druk1(KDL::JntArray q_init,KDL::ChainIkSolverPos_NR_JL kdl_solver){//

    //robimy trajektorie

    Frame 	F_base_start(Frame(Rotation::RPY(0,0,0), Vector(0,0,0)));
    Frame   F_base_end(Frame(Rotation::RPY(0,0,0), Vector(78,11,22)));
    KDL::RotationalInterpolation *interRot= new RotationalInterpolation_SingleAxis();
    KDL::VelocityProfile_Trap *vel_profile = new KDL::VelocityProfile_Trap(1,1);
    KDL::Path_Line *pathXYZ = new Path_Line(F_base_start,F_base_end,interRot,0.01,true);//nie do koń☺ca rozumiem wszystko w tych interpolacjach ale działa niby liniowa
    std::cout<<"dlugosc_trasy_wynosi="<<pathXYZ->PathLength()<<std::endl;
    vel_profile->SetProfile(0,pathXYZ->PathLength());//brak mozliwości rozpisania na kroki trzeba robic trajektorie wg profilu predkosci
    KDL::Trajectory_Segment *traj = new KDL::Trajectory_Segment(pathXYZ,vel_profile);

    std::cout<<"czas trwania="<<traj->Duration()<<std::endl;//czas trwania....w zasadzie można stad wyciagnać ilość kroków patrz nizej dt
    //std::cout<<"kroczki="<<std::endl;
    KDL::JntArray  desired_q(chain.getNrOfJoints());
    q_init=desired_q;
    //////////
    double dt=0.1;


          //  std::ofstream of("./trajectory.dat");
            for (double t=0.0; t <= traj->Duration(); t+= dt) {
                QEventLoop loop;
                QTimer::singleShot(100, &loop, SLOT(quit()));//co 0,1s daje kolejną pozycje tylko tak na teraz
                loop.exec();
                KDL::Frame current_pose;//aktualna pozycja
                current_pose = traj->Pos(t);
                //double x,y,z;

           int status = kdl_solver.CartToJnt(q_init,current_pose ,desired_q);//rozwiązanie odwrotne dla danej chwili
           q_init=desired_q;
           std::cout << status<<"jest fajnie w luj"<<std::endl;
           for(int i=0;i<int(chain.getNrOfJoints());i++)
           std::cout << desired_q(i)<<std::endl;


            }




}


void MainWindow::on_pobierz_clicked()//test liczenia kinematyki odwrotnej
{
    // Create solver based on kinematic chain
std::cout <<"sdddd";
    // Create joint array
    unsigned int nj = chain.getNrOfJoints();
  //  KDL::JntArray jointpositions = JntArray(nj);
     KDL::JntArray q_min2(nj);
     KDL::JntArray q_max2(nj);
     for(int i=0;i<int(nj);i++){
         q_min2(i)=q_min[i];
         q_max2(i)=q_max[i];
     }

    double eps = 1e-5;
std::cout <<"sdddd";
    KDL::ChainFkSolverPos_recursive fk_solver2(chain); // Forward kin. solver
    KDL::ChainIkSolverVel_pinv vik_solver(chain); // PseudoInverse vel solver
    KDL::ChainIkSolverPos_NR_JL kdl_solver(chain,q_min2,q_max2,fk_solver2, vik_solver, 100,eps);//nasz wspanialy solwer


    std::cout <<"test dla lancucha kinematycznego o nst. liczbie osi"<<chain.getNrOfJoints()<<std::endl;//komunikat o ilości ogniw łańcucha w luj
   // KDL::JntArray q(chain.getNrOfJoints());//
    KDL::JntArray desired_q(chain.getNrOfJoints());//końcowe współrzędne masznowe
    KDL::JntArray q_init(chain.getNrOfJoints());
          for(int i=0; i <int(chain.getNrOfJoints()); i++ ) {  //dodajemy  początkowe wsp. maszynowe for fun
              q_init(i) = (q_max2(i)-q_min2(i))/3;
          }
          std::cout << q_init(0)<<std::endl;
          std::cout << q_init(1)<<std::endl;
          std::cout << q_init(2)<<std::endl;

   KDL::Frame desired_position;//ramka z odpowiedziami

         desired_position.p.x(78);//zadane wartości
         desired_position.p.y(11);
         desired_position.p.z(22);


    int status = kdl_solver.CartToJnt(q_init,desired_position,desired_q);//


   if(status >=0){

    std::cout << status<<"  jest fajnie"<<std::endl;
    for(int i=0;i<int(nj);i++)
    std::cout << desired_q(i)<<std::endl;
   // std::cout << desired_q(3)<<std::endl;
}
   else{
   std::cout << status<<"  wystapil blad...mozna isc umierac"<<std::endl;}




    if(status>=0){
               ui->informacje->append("%s \n Succes, thanks KDL!");
    }else{
        ui->informacje->append("%s \n Error: could not calculate forward kinematics :(");
    }

  druk1(q_init,kdl_solver);


}

/*void MainWindow::on_pobierz_clicked(bool checked)
{

}*/

void MainWindow::on_OX_clicked()
{
    chain.addSegment(Segment(Joint(Joint::TransX)));
    opis_chain=opis_chain+", X_R";
    ui->Opis_chain_kin->clear();
    ui->Opis_chain_kin->append(opis_chain);
  if(chain.getNrOfJoints()>0)
      ui->endChain->setEnabled(true);

}

void MainWindow::on_OY_clicked()
{
    chain.addSegment(Segment(Joint(Joint::TransY)));
    opis_chain=opis_chain+", Y_R";
    ui->Opis_chain_kin->clear();
    ui->Opis_chain_kin->append(opis_chain);
  if(chain.getNrOfJoints()>0)
       ui->endChain->setEnabled(true);
}

void MainWindow::on_OZ_clicked()
{
    chain.addSegment(Segment(Joint(Joint::TransZ)));
    opis_chain=opis_chain+", Z_R";
    ui->Opis_chain_kin->clear();
    ui->Opis_chain_kin->append(opis_chain);
  if(chain.getNrOfJoints()>0)
      ui->endChain->setEnabled(true);
}

void MainWindow::on_endChain_clicked()
{
    opis_chain=opis_chain+"}";
    ui->Opis_chain_kin->clear();
    ui->Opis_chain_kin->append(opis_chain);
      ui->OX->setEnabled(false);
      ui->OY->setEnabled(false);
      ui->OZ->setEnabled(false);
      ui->endChain->setEnabled(false);
      //unsigned int n=;
     // int numb=int(n);
      //q_min=new double[chain.getNrOfJoints()];
    //  q_max=new double[chain.getNrOfJoints()];
}



void MainWindow::on_SetLim_clicked()
{//wczytuje limity Pozycji dla kolejnych członów
    string smin,smax;
    double min,max;
    std::string::size_type sz,sz2;
    smin=ui->Qmin->text().toStdString();
    smax=ui->Qmax->text().toStdString();
    min= std::stod (smin,&sz);
    max= std::stod (smax,&sz2);
    std::cout<<min<<max<<std::endl;
    q_min.push_back(min);
    q_max.push_back(max);
    li++;
    std::cout<<"dfsfd"<<std::endl;
    if(li==int(chain.getNrOfJoints())){
        ui->SetLim->setEnabled(false);
    li=0;}
    ui->Qmin->clear();
    ui->Qmax->clear();
}



void MainWindow::on_GetChainFromXml_clicked()
{
    ui->MakeOwnChain->setEnabled(false);
    //The QDomDocument class represents an XML document.
    QDomDocument xmlDom;
    // ___________________________
    // ::: Read data from file :::


    // Load xml file as raw data
    QFile f("kinematicchain.xml");
    f.open(QIODevice::ReadOnly );
    if (!f.isOpen())
    {
        // Error while loading file
        std::cerr << "Error while loading file" << std::endl;
       // return 1;
    }
    else {
        std::cerr << "lecimy tutaj" << std::endl;
    }
    // Set data into the QDomDocument before processing
     xmlDom.setContent(&f);
    f.close();
    // Extract the root markup
    QDomElement root=xmlDom.documentElement();
    // Get root names and attributes
    QString Type=root.tagName();
    QString Board=root.attribute("BOARD","No name");
    int Year=root.attribute("YEAR","1410").toInt();//jakby ktoś chciał robić samogon

    // Get the first child of the root (Markup COMPONENT is expected)
    QDomElement Component=root.firstChild().toElement();
    while(!Component.isNull())
    {

        // Check if the child tag name is COMPONENT
        if (Component.tagName()=="COMPONENT")
        {

            // Read and display the component ID
            QString ID=Component.attribute("ID","No ID");

            // Get the first child of the component
            QDomElement Child=Component.firstChild().toElement();

            QString Name;
            double LimMin,LimMax;

            // Read each child of the component node
            while (!Child.isNull())
            {
                // Read Name and value
                if (Child.tagName()=="NAME") Name=Child.firstChild().toText().data();
                if (Child.tagName()=="LimMin")  LimMin=Child.firstChild().toText().data().toDouble();
                 if (Child.tagName()=="LimMax") LimMax=Child.firstChild().toText().data().toDouble();

                // Next child
                Child = Child.nextSibling().toElement();
            }
            string Type=Name.toStdString().c_str();
            // Display component data
            std::cout << "Component " << ID.toStdString().c_str() << std::endl;
            std::cout << "   Name  = " << Name.toStdString().c_str() << std::endl;
            std::cout << "   LimMin = " << LimMin << std::endl;
            std::cout << "   LimMax = " << LimMax << std::endl;
            std::cout << std::endl;
            //tworzenie łańcuchów oraz zakresów
            if(Type=="TransX")
            chain.addSegment(Segment(Joint(Joint::TransX)));
            if(Type=="TransY")
            chain.addSegment(Segment(Joint(Joint::TransY)));
            if(Type=="TransZ")
            chain.addSegment(Segment(Joint(Joint::TransZ)));
            q_min.push_back(LimMin);
            q_max.push_back(LimMax);
        }

 std::cout << "   SPR ilosc osi" << chain.getNrOfSegments()<< std::endl;
  std::cout << "   SPR ilosc min" << q_min.size()<< std::endl;
   std::cout << "   SPR  ilosc max"<< q_max.size()<< std::endl;
        // Next component
        Component = Component.nextSibling().toElement();
    }


//Arrivadercii blokada
 ui->GetChainFromXml->setEnabled(false);
}

void MainWindow::on_MakeOwnChain_clicked()
{
    ui->GetChainFromXml->setEnabled(false);
    ui->OX->setEnabled(true);
    ui->OY->setEnabled(true);
    ui->OZ->setEnabled(true);
    ui->endChain->setEnabled(true);
}

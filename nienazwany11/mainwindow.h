#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <chainiksolvervel_pinv.hpp>
//#include <chainiksolvervel_pinv_givens.hpp> Do drzewa

#include <kdl/frames_io.hpp>
#include<rotational_interpolation.hpp>
#include<rotational_interpolation_sa.hpp>
#include <path_line.hpp>
#include <stdio.h>
#include <iostream>
//include <trajectory_utils/trajectory_utils.h>
#include<velocityprofile_trap.hpp>
#include<trajectory_segment.hpp>
#include<QtCore>

using namespace KDL;
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    KDL::Chain chain;
    vector <double>q_min;
    vector <double>q_max;
    int li=0;

private slots:

   void druk1(KDL::JntArray q_init,KDL::ChainIkSolverPos_NR_JL kdl_solver);
   void on_pobierz_clicked();

   //void on_pobierz_clicked(bool checked);

   void on_OX_clicked();

   void on_OY_clicked();

   void on_OZ_clicked();

   void on_endChain_clicked();

  // void on_pushButton_clicked();

   void on_SetLim_clicked();



   void on_GetChainFromXml_clicked();

   void on_MakeOwnChain_clicked();

private:
    Ui::MainWindow *ui;
    QString opis_chain="{";
};

#endif // MAINWINDOW_H

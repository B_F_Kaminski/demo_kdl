/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTextBrowser *informacje;
    QPushButton *pobierz;
    QLineEdit *odczyt;
    QPushButton *OX;
    QPushButton *OY;
    QPushButton *OZ;
    QPushButton *endChain;
    QTextBrowser *Opis_chain_kin;
    QPushButton *SetLim;
    QLabel *label;
    QLineEdit *Qmin;
    QLineEdit *Qmax;
    QPushButton *GetChainFromXml;
    QPushButton *MakeOwnChain;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(646, 512);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        informacje = new QTextBrowser(centralWidget);
        informacje->setObjectName(QStringLiteral("informacje"));
        informacje->setGeometry(QRect(40, 150, 451, 41));
        pobierz = new QPushButton(centralWidget);
        pobierz->setObjectName(QStringLiteral("pobierz"));
        pobierz->setGeometry(QRect(510, 150, 101, 21));
        odczyt = new QLineEdit(centralWidget);
        odczyt->setObjectName(QStringLiteral("odczyt"));
        odczyt->setGeometry(QRect(520, 200, 71, 31));
        OX = new QPushButton(centralWidget);
        OX->setObjectName(QStringLiteral("OX"));
        OX->setGeometry(QRect(50, 70, 75, 23));
        OY = new QPushButton(centralWidget);
        OY->setObjectName(QStringLiteral("OY"));
        OY->setGeometry(QRect(130, 70, 75, 23));
        OZ = new QPushButton(centralWidget);
        OZ->setObjectName(QStringLiteral("OZ"));
        OZ->setGeometry(QRect(210, 70, 75, 23));
        endChain = new QPushButton(centralWidget);
        endChain->setObjectName(QStringLiteral("endChain"));
        endChain->setGeometry(QRect(290, 70, 291, 23));
        Opis_chain_kin = new QTextBrowser(centralWidget);
        Opis_chain_kin->setObjectName(QStringLiteral("Opis_chain_kin"));
        Opis_chain_kin->setGeometry(QRect(110, 10, 271, 41));
        SetLim = new QPushButton(centralWidget);
        SetLim->setObjectName(QStringLiteral("SetLim"));
        SetLim->setGeometry(QRect(320, 120, 75, 23));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 100, 131, 16));
        Qmin = new QLineEdit(centralWidget);
        Qmin->setObjectName(QStringLiteral("Qmin"));
        Qmin->setGeometry(QRect(40, 120, 113, 20));
        Qmax = new QLineEdit(centralWidget);
        Qmax->setObjectName(QStringLiteral("Qmax"));
        Qmax->setGeometry(QRect(170, 120, 113, 20));
        GetChainFromXml = new QPushButton(centralWidget);
        GetChainFromXml->setObjectName(QStringLiteral("GetChainFromXml"));
        GetChainFromXml->setGeometry(QRect(400, 10, 181, 21));
        MakeOwnChain = new QPushButton(centralWidget);
        MakeOwnChain->setObjectName(QStringLiteral("MakeOwnChain"));
        MakeOwnChain->setGeometry(QRect(400, 30, 181, 21));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 646, 34));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        pobierz->setText(QApplication::translate("MainWindow", "pobierz", Q_NULLPTR));
        OX->setText(QApplication::translate("MainWindow", "O\305\233 \"X\"", Q_NULLPTR));
        OY->setText(QApplication::translate("MainWindow", "O\305\233 \"Y\"", Q_NULLPTR));
        OZ->setText(QApplication::translate("MainWindow", "O\305\233 \"Z\"", Q_NULLPTR));
        endChain->setText(QApplication::translate("MainWindow", "zako\305\204cz tworzenie \305\202a\305\204cucha kinematycznego", Q_NULLPTR));
        SetLim->setText(QApplication::translate("MainWindow", "zatwierd\305\272", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Zakres ruchu osi min/max", Q_NULLPTR));
        GetChainFromXml->setText(QApplication::translate("MainWindow", "Za\305\202aduj \305\202a\305\204cuch z Xml", Q_NULLPTR));
        MakeOwnChain->setText(QApplication::translate("MainWindow", "Stw\303\263rz w\305\202asny", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
